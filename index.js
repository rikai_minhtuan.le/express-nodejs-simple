import express from "express";
import connectToDb from "./src/db/connect";
import config from "./src/config";
import cors from 'cors';

const app = express();
connectToDb(config.database);
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.listen(config.port, (error) => {
  if (error) {
    console.log(error);
  }
  console.log(`app is listening on port ${config.port}`);
  console.log(config.database);
});
