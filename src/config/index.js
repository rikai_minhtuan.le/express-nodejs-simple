import * as dotenv from "dotenv";

dotenv.config();
const nodeEnv = process.env.NODE_ENV;
let fileName = "";

if (nodeEnv === "local" || !nodeEnv) {
  fileName = ".env";
}

const app = {
  database: `mongodb://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`,
  host: process.env.HOST,
  port: process.env.PORT,
};

export default app;
